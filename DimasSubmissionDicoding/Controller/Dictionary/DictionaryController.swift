//
//  ViewController.swift
//  DimasDicodingSubmission
//
//  Created by Dimas A. Prabowo on 23/07/21.
//

import UIKit
import CoreData

class DictionaryController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    let context = DataManager.shared.context
    static var sound = true
    private var searchHistory = [WordData]()
    private let welcomeMessages = ["teach", "us", "how", "to", "be", "the", "best", "programmer", "in", "the"]
    private var wordData = [Word]()
    
    let searchController = UISearchController()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.searchController.isActive = false
        self.title = "URDict"
        self.searchController.searchBar.placeholder = "Translate"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        fetchItems()
        
        if searchHistory.count > 11 {
            let sortedWord = searchHistory.sorted {
                guard let prev = $0.searchDate,
                      let next = $1.searchDate else { return true }
                return prev > next
            }
            self.searchHistory = sortedWord
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchItems()
        
        if searchHistory.isEmpty {
            DispatchQueue.main.async {
                self.present(LoadingController(message: "Initializing...").createLoading(), animated: true, completion: nil)
            }
            
            initCoreData()
        }
        
        self.searchController.searchBar.delegate = self
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.register(SearchHistoryCell.nib(), forCellReuseIdentifier: SearchHistoryCell.identifier)
    }
    
    private func prepareData() {
        let sortedWord = searchHistory.sorted {
            return $0.id < $1.id
        }
        
        self.searchHistory = sortedWord
    }
    
    private func initCoreData() {
        for (id, text) in welcomeMessages.enumerated() {
            getData(urlString: "\(url)\(text)", id: id)
        }
        getData(urlString: "\(url)world", id: welcomeMessages.count + 1, last: true)
    }
    
    private func setBackgroundColor(id: Int) -> UIColor {
        if (id + 1) % 5 == 0 {
            return Colors.yellow
        } else if (id + 1) % 4 == 0 {
            return Colors.blue
        } else if (id + 1) % 3 == 0 {
            return Colors.green
        } else if (id + 1) % 2 == 0 {
            return Colors.darkBlue
        } else {
            return Colors.red
        }
    }
    
    // MARK: - Get Data
    private func getData(urlString: String, id: Int, last: Bool = false) {
        guard let url = URL(string: urlString) else {
            print("wrong URL Format")
            return
        }
        
        URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
            guard let data = data, error == nil else {
                print("Data not found")
                return
            }
            
            var result = [Word]()
            do {
                result = try JSONDecoder().decode([Word].self, from: data)
                
                if let data = result.first {
                    let word: WordData = WordData(context: self!.context)
                    word.word = data.word
                    word.id = Int64(id)
                    word.searchDate = Date(timeIntervalSinceNow: 0)
                    
                    // Phonetics
                    if let phonetics = data.phonetics {
                        for phonetic in phonetics {
                            let phone = PhoneticData(context: self!.context)
                            phone.audioString = phonetic.audio
                            phone.text = phonetic.text
                            word.addToMyPhonetics(phone)
                        }
                    }
                    
                    // Meanings
                    if let meanings = data.meanings {
                        for meaning in meanings {
                            let mean = MeaningData(context: self!.context)
                            mean.partOfSpeech = meaning.partOfSpeech
                            
                            for definition in meaning.definitions {
                                let define = DefinitionData(context: self!.context)
                                define.definition = definition.definition
                                define.example = definition.example
                                
                                mean.addToMyDefinitions(define)
                            }
                            word.addToMyMeanings(mean)
                        }
                    }
                    
                    DataManager.shared.saveData()
                }
                
                if last {
                    self?.fetchItems()
                    self?.prepareData()
                    
                    DispatchQueue.main.async {
                        self?.dismiss(animated: false, completion: nil)
                        self?.tableView.reloadData()
                    }
                }
            } catch {
                print("failed to convert: \(error.localizedDescription)")
            }
            
        }.resume()
    }
    
    func formatDate() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        return formatter
    }
}

// MARK: Fetch Core Data
extension DictionaryController: CoreDataFetchDelegate {
    func fetchItems() {
        do {
            let request: NSFetchRequest<WordData> = WordData.fetchRequest()
            request.returnsObjectsAsFaults = false
            self.searchHistory = try context.fetch(request)
        } catch {
            print(error)
        }
    }
}

// MARK: Search Controller
extension DictionaryController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let resultController = ResultController(nibName: ResultController.identifier, bundle: nil)
        
        if let key = searchBar.text {
            resultController.configure(with: key)
            self.navigationController?.pushViewController(resultController, animated: true)
        }
    }
}

// MARK: Table View
extension DictionaryController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchHistory.count > 11 {
            return 11
        } else {
            return searchHistory.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: SearchHistoryCell.identifier, for: indexPath) as? SearchHistoryCell {
            cell.word.text = searchHistory[indexPath.row].word
            
            cell.background.backgroundColor = setBackgroundColor(id: indexPath.row)
            if let date = searchHistory[indexPath.row].searchDate {
                cell.searchDate.text = "Search on \(formatDate().string(from: date))"
            }
            if let phonetics = searchHistory[indexPath.row].myPhonetics?.allObjects as? [PhoneticData],
               let phonetic = phonetics.first?.text {
                cell.phonetic.text = phonetic
            }
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        let resultController = ResultController(nibName: ResultController.identifier, bundle: nil)
        if let keyword = searchHistory[indexPath.row].word {
            resultController.configure(with: keyword)
            self.navigationController?.pushViewController(resultController, animated: true)
        }
    }
}

