//
//  DetailWordCell.swift
//  DimasDicodingSubmission
//
//  Created by Dimas A. Prabowo on 28/07/21.
//

import UIKit

class DetailWordCell: UITableViewCell {
    @IBOutlet weak var word: UILabel!
    
    static let identifier = "DetailWordCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "DetailWordCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
