//
//  Colors.swift
//  DimasSubmissionDicoding
//
//  Created by Dimas A. Prabowo on 28/07/21.
//

import Foundation
import UIKit

struct Colors {
    static let red = UIColor(rgb: 0xFFC1A4)
    static let darkBlue = UIColor(rgb: 0x7C87FF)
    static let green = UIColor(rgb: 0x79F1BC)
    static let blue = UIColor(rgb: 0x9AD5F4)
    static let yellow = UIColor(rgb: 0xFFE084)
    static let pacificBlue = UIColor(rgb: 0x304C62)
}

extension UIColor {
   convenience init(red: Int, green: Int, blue: Int) {
       assert(red >= 0 && red <= 255, "Invalid red component")
       assert(green >= 0 && green <= 255, "Invalid green component")
       assert(blue >= 0 && blue <= 255, "Invalid blue component")

       self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
   }

   convenience init(rgb: Int) {
       self.init(
           red: (rgb >> 16) & 0xFF,
           green: (rgb >> 8) & 0xFF,
           blue: rgb & 0xFF
       )
   }
}
