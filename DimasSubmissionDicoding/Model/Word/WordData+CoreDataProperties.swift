//
//  WordData+CoreDataProperties.swift
//  DimasSubmissionDicoding
//
//  Created by Dimas A. Prabowo on 30/07/21.
//
//

import Foundation
import CoreData


extension WordData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WordData> {
        return NSFetchRequest<WordData>(entityName: "WordData")
    }

    @NSManaged public var searchDate: Date?
    @NSManaged public var word: String?
    @NSManaged public var id: Int64
    @NSManaged public var myMeanings: NSSet?
    @NSManaged public var myPhonetics: NSSet?

}

// MARK: Generated accessors for myMeanings
extension WordData {

    @objc(addMyMeaningsObject:)
    @NSManaged public func addToMyMeanings(_ value: MeaningData)

    @objc(removeMyMeaningsObject:)
    @NSManaged public func removeFromMyMeanings(_ value: MeaningData)

    @objc(addMyMeanings:)
    @NSManaged public func addToMyMeanings(_ values: NSSet)

    @objc(removeMyMeanings:)
    @NSManaged public func removeFromMyMeanings(_ values: NSSet)

}

// MARK: Generated accessors for myPhonetics
extension WordData {

    @objc(addMyPhoneticsObject:)
    @NSManaged public func addToMyPhonetics(_ value: PhoneticData)

    @objc(removeMyPhoneticsObject:)
    @NSManaged public func removeFromMyPhonetics(_ value: PhoneticData)

    @objc(addMyPhonetics:)
    @NSManaged public func addToMyPhonetics(_ values: NSSet)

    @objc(removeMyPhonetics:)
    @NSManaged public func removeFromMyPhonetics(_ values: NSSet)

}

extension WordData : Identifiable {

}
