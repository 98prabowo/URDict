//
//  Word.swift
//  DimasDicodingSubmission
//
//  Created by Dimas A. Prabowo on 28/07/21.
//

import Foundation

let url = "https://api.dictionaryapi.dev/api/v2/entries/en_US/"

struct Word: Codable {
    let word: String
    let origin: String?
    var date: Date?
    let phonetics: [Phonetic]?
    let meanings: [Meaning]?
    
    enum CodingKeys: String, CodingKey {
        case word, origin, phonetics, meanings
    }
}

struct Phonetic: Codable {
    let text: String
    let audio: String?
}

struct Meaning: Codable {
    let partOfSpeech: String
    let definitions: [Definition]
}

struct Definition: Codable {
    let definition: String
    let example: String?
    let synonyms: [String]?
    let antonyms: [String]?
}
