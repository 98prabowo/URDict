//
//  CoreDataFetchDelegate.swift
//  DimasSubmissionDicoding
//
//  Created by Dimas A. Prabowo on 29/07/21.
//

import Foundation
import CoreData

protocol CoreDataFetchDelegate {
    func fetchItems()
}
