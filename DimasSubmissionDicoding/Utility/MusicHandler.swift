//
//  Music.swift
//  DimasSubmissionDicoding
//
//  Created by Dimas A. Prabowo on 28/07/21.
//

import Foundation
import AVFoundation

class MusicHandler {
    static let shared = MusicHandler()
    
    var audioPlayer: AVPlayer?
    
    func playSoundEffect(sound: String) {
        let url: URL? = URL(string: "https:\(sound)")
        guard let urlSound = url else { return }
        audioPlayer = AVPlayer(url: urlSound)
        audioPlayer?.volume = 1.0
        audioPlayer?.play()
    }
}
