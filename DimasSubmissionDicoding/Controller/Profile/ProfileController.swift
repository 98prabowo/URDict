//
//  Profile.swift
//  DimasSubmissionDicoding
//
//  Created by Dimas A. Prabowo on 28/07/21.
//

import Foundation
import UIKit

class ProfileController: UIViewController {
    @IBOutlet weak var image: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.image.layer.cornerRadius = 20
    }
}
