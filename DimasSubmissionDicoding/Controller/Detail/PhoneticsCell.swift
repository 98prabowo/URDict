//
//  PhoneticsCell.swift
//  DimasDicodingSubmission
//
//  Created by Dimas A. Prabowo on 28/07/21.
//

import UIKit

class PhoneticsCell: UITableViewCell {
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var phonetic: UILabel!
    @IBOutlet weak var playButton: UIButton!
    
    private var phoneticData: Phonetic?
    
    static let identifier = "PhoneticsCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "PhoneticsCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.background.layer.cornerRadius = 20
        self.background.backgroundColor = Colors.yellow
        self.playButton.tintColor = Colors.pacificBlue
        
        if DictionaryController.sound {
            playButton.isEnabled = true
        } else {
            playButton.isEnabled = false
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(with phonetic: Phonetic) {
        self.phoneticData = phonetic
    }
    
    @IBAction func playTapped(_ sender: Any) {
        if let data = phoneticData?.audio {
            MusicHandler.shared.playSoundEffect(sound: data)
        }
    }
}
