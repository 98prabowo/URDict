//
//  MeaningData+CoreDataProperties.swift
//  DimasSubmissionDicoding
//
//  Created by Dimas A. Prabowo on 29/07/21.
//
//

import Foundation
import CoreData


extension MeaningData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MeaningData> {
        return NSFetchRequest<MeaningData>(entityName: "MeaningData")
    }

    @NSManaged public var partOfSpeech: String?
    @NSManaged public var myDefinitions: NSSet?
    @NSManaged public var meaningWord: WordData?

}

// MARK: Generated accessors for myDefinitions
extension MeaningData {

    @objc(addMyDefinitionsObject:)
    @NSManaged public func addToMyDefinitions(_ value: DefinitionData)

    @objc(removeMyDefinitionsObject:)
    @NSManaged public func removeFromMyDefinitions(_ value: DefinitionData)

    @objc(addMyDefinitions:)
    @NSManaged public func addToMyDefinitions(_ values: NSSet)

    @objc(removeMyDefinitions:)
    @NSManaged public func removeFromMyDefinitions(_ values: NSSet)

}

extension MeaningData : Identifiable {

}
