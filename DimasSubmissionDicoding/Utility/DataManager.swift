//
//  DataManager.swift
//  DimasSubmissionDicoding
//
//  Created by Dimas A. Prabowo on 28/07/21.
//

import Foundation
import UIKit
import CoreData

class DataManager {
    static let shared = DataManager()

    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func saveData() {
        do {
            try context.save()
        } catch {
            print(error)
        }
    }
}
