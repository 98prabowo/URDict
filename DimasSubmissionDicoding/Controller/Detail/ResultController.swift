//
//  ResultController.swift
//  DimasDicodingSubmission
//
//  Created by Dimas A. Prabowo on 28/07/21.
//

import UIKit

class ResultController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private let context = DataManager.shared.context
    private var word = [Word]()
    private var text: String?
    
    static let identifier = "ResultController"
    
    static func nib() -> UINib {
        return UINib(nibName: "ResultController", bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            self.present(LoadingController(message: "Translating...").createLoading(), animated: true, completion: nil)
        }
        
        if let key = text {
            getData(urlString: "\(url)\(key)")
        }
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(DetailWordCell.nib(), forCellReuseIdentifier: DetailWordCell.identifier)
        self.tableView.register(PhoneticsCell.nib(), forCellReuseIdentifier: PhoneticsCell.identifier)
        self.tableView.register(DetailHeaderCell.nib(), forCellReuseIdentifier: DetailHeaderCell.identifier)
        self.tableView.register(MeaningsCell.nib(), forCellReuseIdentifier: MeaningsCell.identifier)
    }
    
    func configure(with text: String) {
        self.text = text.lowercased()
    }
    
    private func setBackgroundColor(id: Int) -> UIColor {
        if (id + 1) % 5 == 0 {
            return Colors.yellow
        } else if (id + 1) % 4 == 0 {
            return Colors.blue
        } else if (id + 1) % 3 == 0 {
            return Colors.green
        } else if (id + 1) % 2 == 0 {
            return Colors.darkBlue
        } else {
            return Colors.red
        }
    }
    
    // MARK: - Get Data
    private func getData(urlString: String) {
        guard let url = URL(string: urlString) else {
            print("wrong URL Format")
            return
        }
        
        URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
            guard let data = data, error == nil else {
                print("Data not found")
                return
            }
            
            var result = [Word]()
            do {
                result = try JSONDecoder().decode([Word].self, from: data)
                self?.word = result
                
                if let data = result.first {
                    let word: WordData = WordData(context: self!.context)
                    word.word = data.word
                    word.id = 0
                    word.searchDate = Date(timeIntervalSinceNow: 0)
                    
                    // Phonetics
                    if let phonetics = data.phonetics {
                        for phonetic in phonetics {
                            let phone = PhoneticData(context: self!.context)
                            phone.audioString = phonetic.audio
                            phone.text = phonetic.text
                            word.addToMyPhonetics(phone)
                        }
                    }
                    
                    // Meanings
                    if let meanings = data.meanings {
                        for meaning in meanings {
                            let mean = MeaningData(context: self!.context)
                            mean.partOfSpeech = meaning.partOfSpeech
                            
                            for definition in meaning.definitions {
                                let define = DefinitionData(context: self!.context)
                                define.definition = definition.definition
                                define.example = definition.example
                                
                                mean.addToMyDefinitions(define)
                            }
                            word.addToMyMeanings(mean)
                        }
                    }
                    
                    DataManager.shared.saveData()
                }
                
                DispatchQueue.main.async {
                    self?.dismiss(animated: false, completion: nil)
                    self?.tableView.reloadData()
                }
            } catch {
                print("failed to convert: \(error.localizedDescription)")
            }
            
        }.resume()
    }
}

// MARK: Table View
extension ResultController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let data = word.first {
            if let meaningCount = data.meanings?.count,
               let phonetiCount = data.phonetics?.count {
                return meaningCount + phonetiCount + 3
            } else if let meaningCount = data.meanings?.count {
                return meaningCount + 2
            } else if let phoneticCount = data.phonetics?.count {
                return phoneticCount + 2
            } else {
                return 1
            }
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let data = word.first else {
            print("Data not found")
            return UITableViewCell() }
        
        if let meanings = data.meanings,
           let phonetics = data.phonetics {
            switch indexPath.row {
            case 0:
                if let cell = tableView.dequeueReusableCell(withIdentifier: DetailWordCell.identifier, for: indexPath) as? DetailWordCell {
                    cell.word.text = "-\(data.word.uppercased())-"
                    cell.word.textColor = Colors.pacificBlue
                    return cell
                } else {
                    return UITableViewCell()
                }
            case 1:
                if let cell = tableView.dequeueReusableCell(withIdentifier: DetailHeaderCell.identifier, for: indexPath) as? DetailHeaderCell {
                    cell.title.text = "Phonetics"
                    cell.title.textColor = Colors.pacificBlue
                    return cell
                } else {
                    return UITableViewCell()
                }
            case 2...(phonetics.count + 1):
                if let cell = tableView.dequeueReusableCell(withIdentifier: PhoneticsCell.identifier, for: indexPath) as? PhoneticsCell {
                    cell.phonetic.text = phonetics[indexPath.row - 2].text
                    cell.phonetic.textColor = Colors.pacificBlue
                    cell.configure(with: phonetics[indexPath.row - 2])
                    return cell
                } else {
                    return UITableViewCell()
                }
            case phonetics.count + 2:
                if let cell = tableView.dequeueReusableCell(withIdentifier: DetailHeaderCell.identifier, for: indexPath) as? DetailHeaderCell {
                    cell.title.text = "Meanings"
                    cell.title.textColor = Colors.pacificBlue
                    return cell
                } else {
                    return UITableViewCell()
                }
            default:
                if let cell = tableView.dequeueReusableCell(withIdentifier: MeaningsCell.identifier, for: indexPath) as? MeaningsCell {
                    cell.background.backgroundColor = setBackgroundColor(id: indexPath.row - (phonetics.count + 3))
                    cell.partOfSpeech.text = meanings[indexPath.row - (phonetics.count + 3)].partOfSpeech
                    cell.definition.text = meanings[indexPath.row - (phonetics.count + 3)].definitions[0].definition
                    if let example = meanings[indexPath.row - (phonetics.count + 3)].definitions[0].example {
                        cell.example.text = example
                    } else {
                        cell.example.text = "-"
                    }
                    return cell
                } else {
                    return UITableViewCell()
                }
            }
        } else if let meanings = data.meanings {
            switch indexPath.row {
            case 0:
                if let cell = tableView.dequeueReusableCell(withIdentifier: DetailWordCell.identifier, for: indexPath) as? DetailWordCell {
                    cell.word.text = "-\(data.word.uppercased())-"
                    cell.word.textColor = Colors.pacificBlue
                    return cell
                } else {
                    return UITableViewCell()
                }
            case 1:
                if let cell = tableView.dequeueReusableCell(withIdentifier: DetailHeaderCell.identifier, for: indexPath) as? DetailHeaderCell {
                    cell.title.text = "Meanings"
                    cell.title.textColor = Colors.pacificBlue
                    return cell
                } else {
                    return UITableViewCell()
                }
            default:
                if let cell = tableView.dequeueReusableCell(withIdentifier: MeaningsCell.identifier, for: indexPath) as? MeaningsCell {
                    cell.background.backgroundColor = setBackgroundColor(id: indexPath.row - 2)
                    cell.partOfSpeech.text = meanings[indexPath.row - 2].partOfSpeech
                    cell.definition.text = meanings[indexPath.row - 2].definitions[0].definition
                    if let example = meanings[indexPath.row - 2].definitions[0].example {
                        cell.example.text = example
                    } else {
                        cell.example.text = "-"
                    }
                    return cell
                } else {
                    return UITableViewCell()
                }
            }
        } else if let phonetics = data.phonetics {
            switch indexPath.row {
            case 0:
                if let cell = tableView.dequeueReusableCell(withIdentifier: DetailWordCell.identifier, for: indexPath) as? DetailWordCell {
                    cell.word.text = "-\(data.word.uppercased())-"
                    cell.word.textColor = Colors.pacificBlue
                    return cell
                } else {
                    return UITableViewCell()
                }
            case 1:
                if let cell = tableView.dequeueReusableCell(withIdentifier: DetailHeaderCell.identifier, for: indexPath) as? DetailHeaderCell {
                    cell.title.text = "Phonetics"
                    cell.title.textColor = Colors.pacificBlue
                    return cell
                } else {
                    return UITableViewCell()
                }
            default:
                if let cell = tableView.dequeueReusableCell(withIdentifier: PhoneticsCell.identifier, for: indexPath) as? PhoneticsCell {
                    cell.phonetic.text = phonetics[indexPath.row - 2].text
                    cell.phonetic.textColor = Colors.pacificBlue
                    cell.configure(with: phonetics[indexPath.row - 2])
                    return cell
                } else {
                    return UITableViewCell()
                }
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: DetailWordCell.identifier, for: indexPath) as? DetailWordCell {
                cell.word.text = "-\(data.word.uppercased())-"
                cell.word.textColor = Colors.pacificBlue
                return cell
            } else {
                return UITableViewCell()
            }
        }
    }
}
