//
//  LoadingController.swift
//  DimasSubmissionDicoding
//
//  Created by Dimas A. Prabowo on 28/07/21.
//

import Foundation
import  UIKit

class LoadingController {
    var message = ""
    
    init(message: String) {
        self.message = message
    }
    
    func createLoading() -> UIAlertController {
        let alert = UIAlertController(title: nil, message: self.message, preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50 , height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.large
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        
        return alert
    }
}
