//
//  DefinitionData+CoreDataProperties.swift
//  DimasSubmissionDicoding
//
//  Created by Dimas A. Prabowo on 29/07/21.
//
//

import Foundation
import CoreData


extension DefinitionData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DefinitionData> {
        return NSFetchRequest<DefinitionData>(entityName: "DefinitionData")
    }

    @NSManaged public var definition: String?
    @NSManaged public var example: String?
    @NSManaged public var definitionMeaning: MeaningData?

}

extension DefinitionData : Identifiable {

}
