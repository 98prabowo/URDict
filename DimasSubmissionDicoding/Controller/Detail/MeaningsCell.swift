//
//  MeaningsCell.swift
//  DimasSubmissionDicoding
//
//  Created by Dimas A. Prabowo on 28/07/21.
//

import UIKit

class MeaningsCell: UITableViewCell {
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var headerPartOfSpeech: UILabel!
    @IBOutlet weak var partOfSpeech: UILabel!
    @IBOutlet weak var headerDefinition: UILabel!
    @IBOutlet weak var definition: UILabel!
    @IBOutlet weak var headerExample: UILabel!
    @IBOutlet weak var example: UILabel!
    
    var labelColor = Colors.pacificBlue
    var headerColor = UIColor.systemBackground
    
    static let identifier = "MeaningsCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "MeaningsCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.background.layer.cornerRadius = 20
        self.partOfSpeech.textColor = labelColor
        self.headerPartOfSpeech.textColor = headerColor
        self.definition.textColor = labelColor
        self.headerDefinition.textColor = headerColor
        self.example.textColor = labelColor
        self.headerExample.textColor = headerColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
