//
//  DetailHeaderCell.swift
//  DimasDicodingSubmission
//
//  Created by Dimas A. Prabowo on 28/07/21.
//

import UIKit

class DetailHeaderCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    
    static let identifier = "DetailHeaderCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "DetailHeaderCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
