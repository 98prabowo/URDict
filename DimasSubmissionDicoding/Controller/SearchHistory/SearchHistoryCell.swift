//
//  SearchHistoryCell.swift
//  DimasDicodingSubmission
//
//  Created by Dimas A. Prabowo on 28/07/21.
//

import UIKit

class SearchHistoryCell: UITableViewCell {
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var word: UILabel!
    @IBOutlet weak var phonetic: UILabel!
    @IBOutlet weak var searchDate: UILabel!
    
    static let identifier = "SearchHistoryCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "SearchHistoryCell", bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.background.layer.cornerRadius = 20
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
