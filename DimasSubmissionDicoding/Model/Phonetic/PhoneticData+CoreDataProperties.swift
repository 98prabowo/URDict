//
//  PhoneticData+CoreDataProperties.swift
//  DimasSubmissionDicoding
//
//  Created by Dimas A. Prabowo on 29/07/21.
//
//

import Foundation
import CoreData


extension PhoneticData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PhoneticData> {
        return NSFetchRequest<PhoneticData>(entityName: "PhoneticData")
    }

    @NSManaged public var audioString: String?
    @NSManaged public var text: String?
    @NSManaged public var phoneticWord: WordData?

}

extension PhoneticData : Identifiable {

}
