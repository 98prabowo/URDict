//
//  Setting.swift
//  DimasSubmissionDicoding
//
//  Created by Dimas A. Prabowo on 30/07/21.
//

import Foundation
import UIKit

class SettingController: UIViewController {
    @IBOutlet weak var `switch`: UISwitch!
    static var sound: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.switch.isOn = DictionaryController.sound
    }
    
    @IBAction func switchTapped(_ sender: Any) {
        if DictionaryController.sound {
            self.switch.isOn = false
            DictionaryController.sound = false
        } else {
            self.switch.isOn = true
            DictionaryController.sound = true
        }
    }
}
